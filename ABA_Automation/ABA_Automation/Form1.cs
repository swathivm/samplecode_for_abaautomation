﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Xml;
using System.IO;
using log4net;
using log4net.Config;
using ABA_Automation.Strings;
using System.Reflection;

namespace ABA_Automation
{
    public partial class Form1 : Form
    {
        private readonly ILog log = LogManager.GetLogger(typeof(Form1));
        Variables AppValues = new Variables();
      
        public Form1()
        {
            InitializeComponent();
         }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void GoButton_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(UrlTextbox.Text) ||
                UrlTextbox.Text.Equals("about:blank"))
            {
                MessageBox.Show("Enter a valid URL.");
                UrlTextbox.Focus();
                return;
            }
            StartNavigation(UrlTextbox.Text);
        }

        private void StartNavigation(string url)
        {
            if (!url.StartsWith("http://") &&
                !url.StartsWith("https://"))
            {
                url = "http://" + url;
            }
            try
            {
                webBrowser1.ScriptErrorsSuppressed = true;
                webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wb_LoadHomePage);
                webBrowser1.Navigate(new Uri(url));
            }
            catch (System.UriFormatException)
            {
                return;
            }
        }

        private void wb_LoadHomePage(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            HtmlElement name = webBrowser1.Document.GetElementById("userNameBox");
            if (name != null)
            {
                name.InnerText = AppValues.userName;
            }
            HtmlElement pass = webBrowser1.Document.GetElementById("passWordBox");
            if (pass != null)
            {
                pass.InnerText = AppValues.passWord;
            }
            HtmlElement submit = webBrowser1.Document.GetElementById("submit");

            webBrowser1.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(wb_LoadHomePage);
            if (submit != null)
                submit.InvokeMember("click");
            webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wb_DoLogin);

            txtLogArea.Text += get_logMessage("Started with user" + " " + AppValues.userName);
            txtLogArea.Text += get_logMessage("Loaded home page");
            log.Info("Started with user"+" "+AppValues.userName);
            log.Info("Loaded home page successfully");
         }

        private void wb_DoLogin(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            txtLogArea.Text += get_logMessage("Logged in successfully");
            log.Info("Logged in successfully");
            webBrowser1.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(wb_DoLogin);
            webBrowser1.Navigate(AppValues.homePageUrl+AppValues.pendingCertificatesUrl);
            webBrowser1.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(wb_GetPendingCertificates);
      }

        private void wb_GetPendingCertificates(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            txtLogArea.Text += get_logMessage("Pending Certificates found");
            log.Info("Pending Certificates found");
            HtmlElementCollection tables = webBrowser1.Document.GetElementById("ctl00_ContentPlaceHolder1_pnlForm").Children;

            if (tables.Count <= 0) return;
            HtmlElementCollection rows = tables[0].GetElementsByTagName("tr");
            foreach (HtmlElement row in rows)
            {
                HtmlElementCollection cells = row.GetElementsByTagName("td");
                int count = 0;
                foreach (HtmlElement cell in cells)
                {
                    String text = cell.InnerText;
                    if (count == 5)
                    {

                        HtmlElementCollection tables1 = cell.GetElementsByTagName("table");
                        foreach (HtmlElement table1 in tables1)
                        {
                            HtmlElementCollection rows1 = table1.GetElementsByTagName("tr");
                            foreach (HtmlElement row1 in rows1)
                            {
                                HtmlElementCollection columns = row1.GetElementsByTagName("td");
                                foreach (HtmlElement columns1 in columns)
                                {
                                    HtmlElement submit = columns1.Children[0];
                                    //HtmlElement ele=columns1.Children[0].Id
                                  //  columns1.Children[0].InvokeMember("click");
                                    submit.InvokeMember("click");
                                    break;
                                }
                                break;
                            }
                            break;
                        }
                        //HtmlElementCollection cells1 = cell.Children[0].GetElementsByTagName("tr")[0].GetElementsByTagName("td");
                        //foreach (HtmlElement cell1 in cells1)
                        //{
                            

                        //}
                    }
                    count += 1;
                   // break;
                }
            }

        }

        private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            UrlTextbox.Text = webBrowser1.Url.ToString();
        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
           // e.Cancel = true;
        }

        private string get_logMessage(string message)
        {
            return (DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ":" + " " + message + Environment.NewLine);
        }
    }
}
