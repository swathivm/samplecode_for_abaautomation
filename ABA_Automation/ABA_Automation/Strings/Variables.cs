﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABA_Automation.Strings
{
   public class Variables
    {
       public readonly string homePageUrl = 
            System.Configuration.ConfigurationManager.AppSettings["HomePage"];
       public readonly string loginPageUrl = 
           System.Configuration.ConfigurationManager.AppSettings["LoginPage"];
       public readonly string pendingCertificatesUrl = 
           System.Configuration.ConfigurationManager.AppSettings["PendingCertificates"];
       public readonly string approveCertificatesUrl = 
           System.Configuration.ConfigurationManager.AppSettings["ApproveCertificates"];
       public readonly string userName =
           System.Configuration.ConfigurationManager.AppSettings["UserName"];
       public readonly string passWord = 
           System.Configuration.ConfigurationManager.AppSettings["Password"];
    }
}
